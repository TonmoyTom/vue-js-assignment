const Weather = {
    weatherData: async(api, location) => {
        const res = await fetch(api + location);
        const data = res.json();
        console.log('Hello');
        return data
    },
    showWeather(waether) {
        const info = document.getElementById('wather');
        if (!waether.error) {
            info.innerHTML = `<table>
            <tbody>
                <tr>
                    <th>Location</th>
                    <td id="name">${waether.location.name}</td>
                </tr>
                <tr>
                    <th>Date</th>
                    <td id="date">${Weather.changeDate(waether.location.localtime)}</td>
                </tr>
                <tr>
                    <th>Day</th>
                    <td id="bar">${Weather.dayName(waether.location.localtime)} </td>
                </tr>
                <tr>
                    <th>PRECIPITATION</th>
                    <td id="preci">${waether.current.condition.text}</td>
                </tr>
                <tr>
                    <th>HUMIDITY</th>
                    <td id="humid">${waether.current.humidity}%</td>
                </tr>
                <tr>
                    <th>WIND KPH</th>
                    <td id="wing">${waether.current.wind_kph}</td>
                </tr>
                <tr>
                    <th>Temperature</th>
                    <td id="temp">${waether.current.temp_c}°C</td>
                </tr>
            </tbody>
            <tfoot>
                <!-- Pie de la tabla, vacío en este caso -->
            </tfoot>
        </table>`
        }
    },
    handleSubmit: async(api, value) => {
        const location = document.getElementById('location').value;
        if (location != '') {
            const weatherData = await Weather.weatherData(api, location);
            Weather.showWeather(weatherData);
        }
        document.getElementById('location').value = '';
    },
    changeDate(date) {
        let currentDate = new Date(date);
        var formd = currentDate.toDateString();
        return formd;
    },
    dayName(date) {
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const dName = new Date(date);
        return days[dName.getDay()];
    }
}

export { Weather }