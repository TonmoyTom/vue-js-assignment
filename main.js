import './style.css'
import { Weather } from './weather.js'

const apiKey = '703ffe04b52449638a8155522232106';
const apiUrl = `http://api.weatherapi.com/v1/current.json?key=${apiKey}&q=`


document.addEventListener('DOMContentLoaded', function() {
    Weather.handleSubmit(apiUrl);
});

document.getElementById('checkLocation').addEventListener('click', function() {
    Weather.handleSubmit(apiUrl);

});